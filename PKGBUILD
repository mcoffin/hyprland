# Maintainer: Caleb Maclennan <caleb@alerque.com>
# Contributor: ThatOneCalculator <kainoa@t1c.dev>
# Contributor: Brenno Lemos <brenno@syndel.is>
# Contributor: Gabriel Fox <inbox@gabrielfox.dev>

pkgname=hyprland
_pkgver='0.33.1'
pkgver=0.33.1.r0.d74607e4
pkgrel=1
pkgdesc='a highly customizable dynamic tiling Wayland compositor'
arch=(x86_64 aarch64)
url="https://github.com/hyprwm/Hyprland"
license=(BSD)
depends=(cairo
         gcc-libs
         glibc
         glslang
         libdisplay-info
         libdrm
         libglvnd
         libinput
         libliftoff
         libx11
         libxcb
         libxcomposite
         libxfixes
         libxkbcommon
         libxrender
         opengl-driver
         pango
         pixman
         polkit
         seatd
         systemd-libs
         vulkan-icd-loader
         vulkan-validation-layers
         wayland
         wayland-protocols
         xcb-proto
         xcb-util
         xcb-util-errors
         xcb-util-keysyms
         xcb-util-renderutil
         xcb-util-wm
         xorg-xinput
         xorg-xwayland)
depends+=(libdisplay-info.so)
makedepends=(cmake
             gdb
             meson
             ninja
             vulkan-headers
             xorgproto
	     ninja)
source=(
	"$pkgname::git+https://github.com/hyprwm/hyprland.git#tag=v${_pkgver}"
	'wlroots::git+https://gitlab.freedesktop.org/wlroots/wlroots.git#commit=5d639394f3e83b01596dcd166a44a9a1a2583350'
	'hyperland-protocols::git+https://github.com/hyprwm/hyprland-protocols.git#commit=0c2ce70625cb30aef199cb388f99e19a61a6ce03'
	'tracy::git+https://github.com/wolfpld/tracy.git#commit=37aff70dfa50cf6307b3fee6074d627dc2929143'
	'udis86::git+https://github.com/canihavesomecoffee/udis86.git#commit=5336633af70f3917760a6d441ff02d93477b0c86'
	'0001-cmake-Adjust-optimization-level-to-follow-arch-packa.patch'
	'0001-cmake-Adjust-optimization-level-to-follow-arch-packa.patch.sig'
)
b2sums=('SKIP'
        'SKIP'
        'SKIP'
        'SKIP'
        'SKIP'
        'a361ad07e54a9bc36ce0e159a1a8f52dde3070fff7725397136b20d4535496adc81880ef2a8f7f1fa72884bcbd24890a798f2e53e1c27655e29b33a71d067379'
        'SKIP')
validpgpkeys=(
	'E11ECA7E363347D9FC170AA5754A7FF6E30B13E6' # Matt Coffin <mcoffin13@gmail.com>
)

pkgver() {
	git -C "$srcdir/$pkgname" describe --long --tags \
		| sed 's/\([^-]*-\)g/r\1/;s/-/./g' \
		| awk '{ print substr($0, 2); exit 0; }'
}

prepare() {
	cd "$srcdir/$pkgname"
	git submodule init
	git config submodule.wlroots.url "$srcdir/wlroots"
	git config submodule.'subprojects/hyperland-protocols'.url "$srcdir/hyperland-protocols"
	git config submodule.'subprojects/tracy'.url "$srcdir/tracy"
	git -c protocol.file.allow=always submodule update

	patch -Np1 < "$srcdir"/0001-cmake-Adjust-optimization-level-to-follow-arch-packa.patch
	# sed -i -e '/^release:/{n;s/-D/-DCMAKE_SKIP_RPATH=ON -D/}' Makefile
}

build() {
	# -DCMAKE_EXTRA_CXX_FLAGS='-stdlib=libstdc++' \
	# -DCMAKE_EXE_LINKER_FLAGS='-stdlib=stdc++' \
	# -DCMAKE_SHARED_LINKER_FLAGS='-stdlib=stdc++' \
	cmake \
		-G Ninja -B build \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_SKIP_RPATH=ON \
		"$pkgname"
	ninja -C build
}

package() {
	# we only fucking wish
	# DESTDIR="$pkgdir" ninja -C "$srcdir/build" install
	cd "$srcdir/$pkgname"
	find src \( -name '*.h' -o -name '*.hpp' \) -exec install -Dm0644 '{}' "$pkgdir/usr/include/hyprland/{}" \;
	pushd subprojects/wlroots/include
	find . -name '*.h' -exec install -Dm0644 '{}' "$pkgdir/usr/include/hyprland/{}" \;
	cd ../build/include
	find . -name '*.h' -exec install -Dm0644 '{}' "$pkgdir/usr/include/hyprland/wlroots/{}" \;
	popd
	DESTDIR="$pkgdir" \
		zsh -c 'install -Dm0644 -t "$DESTDIR"/usr/include/hyprland/wlroots protocols/*-protocol.h(.)'
	install -Dm0644 -t "$pkgdir/usr/share/pkgconfig" "$srcdir"/build/hyprland.pc
	pushd "$srcdir"/build
	DESTDIR="$pkgdir" zsh -c 'install -Dm755 -t "$DESTDIR"/usr/bin/ ./**/{Hyprland,hyprctl}(*Y1)'
	popd
	# install -Dm0755 -t "$pkgdir/usr/bin/" "$srcdir"/build/Hyprland build/hyprctl/hyprctl
	install -Dm0644 -t "$pkgdir/usr/share/$pkgname/" assets/wall_*.png
	install -Dm0644 -t "$pkgdir/usr/share/wayland-sessions/" "example/$pkgname.desktop"
	install -Dm0644 -t "$pkgdir/usr/share/$pkgname/" "example/$pkgname.conf"
	install -Dm0644 -t "$pkgdir/usr/share/licenses/$pkgname/" LICENSE
	DESTDIR="$pkgdir" \
		zsh -c 'install -Dm755 -t "$DESTDIR"/usr/lib subprojects/wlroots/build/**/libwlroots.so.*(.); exit $?'
}
# _shitty_pkg() {
# 	cd "$_archive"
# 	find src \( -name '*.h' -o -name '*.hpp' \) -exec install -Dm0644 {} "$pkgdir/usr/include/hyprland/{}" \;
# 	pushd subprojects/wlroots/include
# 	find . -name '*.h' -exec install -Dm0644 {} "$pkgdir/usr/include/hyprland/wlroots/{}" \;
# 	popd
# 	pushd subprojects/wlroots/build/include
# 	find . -name '*.h' -exec install -Dm0644 {} "$pkgdir/usr/include/hyprland/wlroots/{}" \;
# 	popd
# 	mkdir -p "$pkgdir/usr/include/hyprland/protocols"
# 	cp protocols/*-protocol.h "$pkgdir/usr/include/hyprland/protocols"
# 	pushd build
# 	cmake -DCMAKE_INSTALL_PREFIX=/usr ..
# 	popd
# 	install -Dm0644 -t "$pkgdir/usr/share/pkgconfig" build/hyprland.pc
# 	install -Dm0755 -t "$pkgdir/usr/bin/" build/Hyprland build/hyprctl/hyprctl
# 	install -Dm0644 -t "$pkgdir/usr/share/$pkgname/" assets/*.png
# 	install -Dm0644 -t "$pkgdir/usr/share/wayland-sessions/" "example/$pkgname.desktop"
# 	install -Dm0644 -t "$pkgdir/usr/share/$pkgname/" "example/$pkgname.conf"
# 	install -Dm0644 -t "$pkgdir/usr/share/licenses/$pkgname/" LICENSE
#         find subprojects/wlroots/build -name 'libwlroots.so.*' -type f -execdir \
#                 install -Dm0755 -t "$pkgdir/usr/lib/" {} \;
# }
